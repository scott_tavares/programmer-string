/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ge.sct.programmer.string;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author stavares
 */
public class ProgrammerStringTest {

  public ProgrammerStringTest() {
  }

  @BeforeClass
  public static void setUpClass() {
  }

  @AfterClass
  public static void tearDownClass() {
  }

  @Before
  public void setUp() {
  }

  @After
  public void tearDown() {
  }

  /**
   * Test of programmerString method, of class ProgrammerString.
   */
  @Test
  public void testProgrammerString() {
    System.out.println("testProgrammerString");

    ProgrammerString instance = new ProgrammerString();

    System.out.println("programmerprogrammer");
    assertEquals(0, instance.programmerString("programmerprogrammer"));

    System.out.println("programmerxprogrammer");
    assertEquals(1, instance.programmerString("programmerxprogrammer"));

    System.out.println("programmerxxxprogrammer");
    assertEquals(3, instance.programmerString("programmerxxxprogrammer"));

    System.out.println("xprogrammerxxxprogrammer");
    assertEquals(3, instance.programmerString("xprogrammerxxxprogrammer"));

    System.out.println("xprxogrammerxxxprogrammer");
    assertEquals(3, instance.programmerString("xprxogrammerxxxprogrammer"));

    System.out.println("xgrxoprammerxxxprogrammer");
    assertEquals(3, instance.programmerString("xgrxoprammerxxxprogrammer"));

    System.out.println("programmexxxprogrammer");
    assertEquals(0, instance.programmerString("programmexxxprogrammer"));

    System.out.println("programmerxxxprogramme");
    assertEquals(0, instance.programmerString("programmerxxxprogramme"));

    System.out.println("<<Math.pow(10, 5)+10>>");
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < Math.pow(10, 5)+10; i++) {
      sb.append("x");
    }
    assertEquals(0, instance.programmerString(sb.toString()));

    System.out.println("<<Empty String>>");
    assertEquals(0, instance.programmerString(""));

  }
}
