package ge.sct.programmer.string;

import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author stavares
 */
public class ProgrammerString {

  public int programmerString(String s) {

    ////////////////////////////////////////////////////////////////////////////
    // Want to fail fast if s is outside the constraint of
    // 1 <= s.length <= 10^5
    if(s.toCharArray().length < 1 || s.toCharArray().length > Math.pow(10, 5))
      return 0;

    try {

      int[] indexRange = new int[2];

      //////////////////////////////////////////////////////////////////////////
      // Get the end index of the first programmer string
      indexRange[0] = findString(s.toCharArray(), "programmer", 0,
          s.toCharArray().length)[1];

      //////////////////////////////////////////////////////////////////////////
      // Get the start index of the second programmer string. Start looking
      // from indexRange[0] + 1 (the end of the first programmer string).
      indexRange[1] = findString(s.toCharArray(), "programmer",
          indexRange[0] + 1, s.toCharArray().length)[0];

      //////////////////////////////////////////////////////////////////////////
      // Return the distance between the two programmer string in the array
      return (indexRange[1] - 1) - (indexRange[0]);

    }catch(Exception e) { // Failed to find one or both programmer strings
      return 0;
    }
  }

  /**
   *
   * @param arr Array to search
   * @param programmerKey Map of counts of each type of char(s) in the string 'programmer'
   * @param i Start search from this index
   * @param j End search at this index
   * @return two item array of the start and end indices of a located programmer string
   * @throws Exception No programmer string was found.
   */
  private int[] findString(char[] arr, String programmerKey,
      int i, int j) throws Exception {

    ////////////////////////////////////////////////////////////////////////////
    // vars
    Map<Character, Integer> programmerKeyMap = new TreeMap<>();
    boolean first = false;
    boolean last = false;
    int[] answer = new int[2];

    ////////////////////////////////////////////////////////////////////////////
    // Prime the keys. Used to keep the count of each type of char(s) in
    // the string.
    for (char c : programmerKey.toLowerCase().toCharArray()) {
      programmerKeyMap
          .put(c, (programmerKeyMap.get(c) == null ? 1 : programmerKeyMap.get(c) + 1));
    }

    for (; i < arr.length; i++) {
      if (programmerKeyMap.get(arr[i]) == null) // the current char is not in the map
        continue;

      if (programmerKeyMap.get(arr[i]) > 1) // decrement the count of the current char
        programmerKeyMap.put(arr[i], programmerKeyMap.get(arr[i]) - 1);
      else // remove the current char from the map because it is the last one of its kind
        programmerKeyMap.remove(arr[i]);

      if (!first) { // the first index of the programmer string found
        answer[0] = i;
        first = true;
      }

      // When the programmerKeyMap becomes empty the the last index of
      // the programmer string has been found.
      if (programmerKeyMap.isEmpty()) {
        answer[1] = i;
        last = true;
        break; // programmer string found
      }
    }

    if (first && last) // the full programmer string found has been found
      return answer;
    else
      throw new Exception("Programmer String Not Found!"); // programmer string not found.
  }
}
